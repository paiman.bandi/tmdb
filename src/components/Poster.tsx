import React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {imgBaseURL} from '../constants/tmdb';

const Poster = ({item, nav, type_}) => {
  console.log(nav);
  return (
    <View style={styles.container} testID="poster">
      <TouchableOpacity onPress={() => nav.navigate('Detail', {item, type_})}>
        {item.poster_path != null ? (
          <Image
            style={styles.card}
            source={{
              uri: imgBaseURL + item.poster_path,
            }}
          />
        ) : (
          <Image
            style={styles.card}
            source={require('../images/not_available.png')}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },

  card: {
    width: 128,
    height: 200,
  },
});

export default Poster;
