import React from 'react';
import {View} from 'react-native';
import {render, waitFor} from '@testing-library/react-native';
import Poster from '../Poster';

jest.mock('../Poster', () => jest.fn());

describe('Poster', () => {
  test('Should render correctly', async () => {
    (Poster as jest.Mock).mockReturnValueOnce(<View testID="mock-poster" />);
    const wrapper = render(<Poster />);

    await waitFor(() => {
      wrapper.getByTestId('mock-poster');
    });
  });
});
