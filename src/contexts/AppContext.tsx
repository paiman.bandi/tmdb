import React from 'react';
import {useIsConnected} from 'react-native-offline';

const AppContext = React.createContext();

export const AppProvider = ({children}) => {
  const isConnected = useIsConnected();
  return (
    <AppContext.Provider value={isConnected}>{children}</AppContext.Provider>
  );
};

export default AppContext;
