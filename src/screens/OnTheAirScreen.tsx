import React, {useContext} from 'react';
import {Text, FlatList, StyleSheet, View} from 'react-native';
import useOnTheAir from '../hooks/useOnTheAir';
import Poster from '../components/Poster';
import AppContext from '../contexts/AppContext';

const OnTheAirScreen = ({nav}) => {
  const [results, resultsLocal, errorMsg] = useOnTheAir();
  const resultsLocalData = JSON.parse(resultsLocal).results;
  const isConnected = useContext(AppContext);

  return (
    <View testID="on-the-air-screen">
      <Text style={styles.textStyle}>On The Air TV Shows</Text>

      {isConnected && errorMsg ? <Text>{errorMsg}</Text> : null}
      <FlatList
        horizontal
        keyExtractor={result => result.id}
        data={isConnected ? results : resultsLocalData}
        renderItem={({item}) => {
          return <Poster item={item} nav={nav} type_="TV Show" />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
    marginTop: 50,
  },
});

export default OnTheAirScreen;
