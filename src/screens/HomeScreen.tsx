import React, {useContext} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import NowPlayingScreen from './NowPlayingScreen';
import MostPopularScreen from './MostPopularScreen';
import TopRatedScreen from './TopRatedScreen';
import OnTheAirScreen from './OnTheAirScreen';
import AppContext from '../contexts/AppContext';
import NetStatus from '../components/NetStatus';

const HomeScreen = ({navigation}) => {
  const isConnected = useContext(AppContext);

  return (
    <SafeAreaView>
      <ScrollView>
        <View testID="home-screen">
          <NetStatus status={isConnected} />
          <NowPlayingScreen nav={navigation} />
          <MostPopularScreen nav={navigation} />
          <TopRatedScreen nav={navigation} />
          <OnTheAirScreen nav={navigation} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
