import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {imgBaseURL} from '../constants/tmdb';

const DetailScreen = ({route, nav}) => {
  return (
    <View testID="detail-screen">
      {route === undefined ? null : (
        <View>
          <Text style={styles.title}>
            {route.params.type_ === 'Movie'
              ? route.params.item.title
              : route.params.item.name}{' '}
          </Text>
          <Image
            style={styles.card}
            source={{
              uri: imgBaseURL + route.params.item.poster_path,
            }}
          />
          <Text style={styles.overview}>{route.params.item.overview} </Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  title: {
    fontSize: 24,
  },
  card: {
    width: 180,
    height: 256,
  },
  overview: {},
});

export default DetailScreen;
