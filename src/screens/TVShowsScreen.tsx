import React, {useContext} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import TopRatedScreen from './TopRatedScreen';
import OnTheAirScreen from './OnTheAirScreen';
import AppContext from '../contexts/AppContext';
import NetStatus from '../components/NetStatus';

const TVShowsScreen = ({navigation}) => {
  const isConnected = useContext(AppContext);
  return (
    <SafeAreaView>
      <ScrollView>
        <View testID="tv-shows-screen">
          <NetStatus status={isConnected} />
          <TopRatedScreen nav={navigation} />
          <OnTheAirScreen nav={navigation} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TVShowsScreen;
