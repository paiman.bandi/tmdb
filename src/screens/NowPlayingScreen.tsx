import React, {useContext} from 'react';
import {Text, FlatList, StyleSheet, View} from 'react-native';
import useNowPlaying from '../hooks/useNowPlaying';
import {imgBaseURL} from '../constants/tmdb';
import Poster from '../components/Poster';
import AppContext from '../contexts/AppContext';

const NowPlayingScreen = ({nav}) => {
  const [results, resultsLocal, errorMsg] = useNowPlaying();
  const resultsLocalData = JSON.parse(resultsLocal).results;
  const isConnected = useContext(AppContext);

  return (
    <View testID="now-playing-screen">
      <Text style={styles.textStyle}>Now Playing Movies</Text>

      {isConnected && errorMsg ? <Text>{errorMsg}</Text> : null}
      <FlatList
        horizontal
        keyExtractor={result => result.id}
        data={isConnected ? results : resultsLocalData}
        renderItem={({item}) => {
          return <Poster item={item} nav={nav} type_="Movie" />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
    marginTop: 50,
  },
});

export default NowPlayingScreen;
