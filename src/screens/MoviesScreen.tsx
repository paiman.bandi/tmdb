import React, {useContext} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import NowPlayingScreen from './NowPlayingScreen';
import MostPopularScreen from './MostPopularScreen';
import AppContext from '../contexts/AppContext';
import NetStatus from '../components/NetStatus';

const MoviesScreen = ({navigation}) => {
  const isConnected = useContext(AppContext);
  return (
    <SafeAreaView>
      <ScrollView>
        <NetStatus status={isConnected} />
        <View testID="movies-screen">
          <NowPlayingScreen nav={navigation} />
          <MostPopularScreen nav={navigation} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default MoviesScreen;
