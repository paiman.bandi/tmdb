import React, {useContext} from 'react';
import {Text, FlatList, StyleSheet, View} from 'react-native';
import useMostPopular from '../hooks/useMostPopular';
import Poster from '../components/Poster';
import AppContext from '../contexts/AppContext';

const MostPopularScreen = ({nav}) => {
  const [results, resultsLocal, errorMsg] = useMostPopular();
  const resultsLocalData = JSON.parse(resultsLocal).results;
  const isConnected = useContext(AppContext);

  return (
    <View testID="most-popular-screen">
      <Text style={styles.textStyle}>Most Popular Movies</Text>
      {isConnected && errorMsg ? <Text>{errorMsg}</Text> : null}
      <FlatList
        horizontal
        keyExtractor={result => result.id}
        data={isConnected ? results : resultsLocalData}
        renderItem={({item}) => {
          return <Poster item={item} nav={nav} type_="Movie" />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
    marginTop: 50,
  },
});

export default MostPopularScreen;
