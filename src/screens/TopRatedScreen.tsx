import React, {useContext} from 'react';
import {Text, FlatList, StyleSheet, View} from 'react-native';
import useTopRated from '../hooks/useTopRated';
import Poster from '../components/Poster';
import AppContext from '../contexts/AppContext';

const TopRatedScreen = ({nav}) => {
  const [results, resultsLocal, errorMsg] = useTopRated();
  const resultsLocalData = JSON.parse(resultsLocal).results;
  const isConnected = useContext(AppContext);

  return (
    <View testID="top-rated-screen">
      <Text style={styles.textStyle}>Top Rated TV Shows</Text>

      {isConnected && errorMsg ? <Text>{errorMsg}</Text> : null}
      <FlatList
        horizontal
        keyExtractor={result => result.id}
        data={isConnected ? results : resultsLocalData}
        renderItem={({item}) => {
          return <Poster item={item} nav={nav} type_="TV Show" />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
    marginTop: 50,
  },
});

export default TopRatedScreen;
