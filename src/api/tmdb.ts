import axios from 'axios';
import {apiKey, apiBaseURL} from '../constants/tmdb';

export default axios.create({
  baseURL: apiBaseURL,
  params: {
    api_key: apiKey,
  },
});
