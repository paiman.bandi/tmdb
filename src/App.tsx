import React from 'react';
import AppNavigator from '../src/navigations/AppNavigator';
import SplashScreen from 'react-native-splash-screen';
import {AppProvider} from '../src/contexts/AppContext';
import {NetworkProvider} from 'react-native-offline';

function App() {
  React.useEffect(() => {
    SplashScreen.hide();
  });

  return <AppNavigator />;
}

export default () => {
  return (
    <NetworkProvider>
      <AppProvider>
        <App />
      </AppProvider>
    </NetworkProvider>
  );
};
