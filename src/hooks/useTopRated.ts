import {useEffect, useState, useContext} from 'react';
import tmdb from '../api/tmdb';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../contexts/AppContext';

export default () => {
  const [results, setResults] = useState([]);
  const [resultsLocal, setResultsLocal] = useState('{ "results": []}');
  const [errorMsg, setErrorMsg] = useState('');
  const [isMounted, setIsMounted] = useState(false);
  const isConnected = useContext(AppContext);

  const fetchTopRated = async () => {
    try {
      const response = await tmdb.get('/tv/top_rated');
      setResults(response.data.results);
      await AsyncStorage.setItem(
        'topRatedMovies',
        JSON.stringify(response.data),
      );
    } catch (err) {
      setErrorMsg(err.toString());
    }
  };

  const fetchTopRatedLocal = async () => {
    try {
      const response = await AsyncStorage.getItem('topRatedMovies');
      if (response !== null) {
        console.log(response);
        setResultsLocal(response);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setIsMounted(true);
    if (isConnected) {
      fetchTopRated();
    }
    fetchTopRatedLocal();
    return () => {
      setIsMounted(false);
    };
  }, []);

  return [results, resultsLocal, errorMsg];
};
