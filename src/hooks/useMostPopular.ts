import {useEffect, useState, useContext} from 'react';
import tmdb from '../api/tmdb';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../contexts/AppContext';

export default () => {
  const [results, setResults] = useState([]);
  const [resultsLocal, setResultsLocal] = useState('{ "results": []}');
  const [errorMsg, setErrorMsg] = useState('');
  const [isMounted, setIsMounted] = useState(false);
  const isConnected = useContext(AppContext);

  const fetchMostPopular = async () => {
    try {
      const response = await tmdb.get('/discover/movie', {
        params: {
          sort_by: 'popularity.desc',
        },
      });
      setResults(response.data.results);

      await AsyncStorage.setItem(
        'mostPopularMovies',
        JSON.stringify(response.data),
      );
    } catch (err) {
      setErrorMsg(err.toString());
    }
  };

  const fetchMostPopularLocal = async () => {
    try {
      const response = await AsyncStorage.getItem('mostPopularMovies');
      if (response !== null) {
        console.log(response);
        setResultsLocal(response);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setIsMounted(true);
    if (isConnected) {
      fetchMostPopular();
    }
    fetchMostPopularLocal();
    return () => {
      setIsMounted(false);
    };
  }, []);

  return [results, resultsLocal, errorMsg];
};
