import {useEffect, useState, useContext} from 'react';
import tmdb from '../api/tmdb';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../contexts/AppContext';

export default () => {
  const [results, setResults] = useState([]);
  const [resultsLocal, setResultsLocal] = useState('{ "results": []}');
  const [errorMsg, setErrorMsg] = useState('');
  const [isMounted, setIsMounted] = useState(false);
  const isConnected = useContext(AppContext);

  const fetchOnTheAir = async () => {
    try {
      const response = await tmdb.get('/tv/on_the_air');
      setResults(response.data.results);
      await AsyncStorage.setItem(
        'onTheAirMovies',
        JSON.stringify(response.data),
      );
    } catch (err) {
      setErrorMsg(err.toString());
    }
  };

  const fetchOnTheAirLocal = async () => {
    try {
      const response = await AsyncStorage.getItem('onTheAirMovies');
      if (response !== null) {
        console.log(response);
        setResultsLocal(response);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setIsMounted(true);
    if (isConnected) {
      fetchOnTheAir();
    }
    fetchOnTheAirLocal();

    return () => {
      setIsMounted(false);
    };
  }, []);

  return [results, resultsLocal, errorMsg];
};
