import {useEffect, useState, useContext} from 'react';
import tmdb from '../api/tmdb';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../contexts/AppContext';

export default () => {
  const [results, setResults] = useState([]);
  const [resultsLocal, setResultsLocal] = useState('{ "results": []}');
  const [errorMsg, setErrorMsg] = useState('');
  const [isMounted, setIsMounted] = useState(false);
  const isConnected = useContext(AppContext);

  const fetchNowPlaying = async () => {
    try {
      const response = await tmdb.get('/discover/movie', {
        params: {
          'primary_release_date.gte': '2021-06-22',
          'primary_release_date.lte': '2021-06-29',
        },
      });
      setResults(response.data.results);
      await AsyncStorage.setItem(
        'nowPlayingMovies',
        JSON.stringify(response.data),
      );
    } catch (err) {
      setErrorMsg(err.toString());
    }
  };

  const fetchNowPlayingLocal = async () => {
    try {
      const response = await AsyncStorage.getItem('nowPlayingMovies');
      if (response !== null) {
        console.log(response);
        setResultsLocal(response);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setIsMounted(true);
    if (isConnected) {
      fetchNowPlaying();
    }
    fetchNowPlayingLocal();

    return () => {
      setIsMounted(false);
    };
  }, []);

  return [results, resultsLocal, errorMsg];
};
