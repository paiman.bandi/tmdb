import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  MoviesStackNavigator,
  HomeStackNavigator,
  TVShowsStackNavigator,
} from './StackNavigator';

const Tab = createBottomTabNavigator();
const TabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeStackNavigator} />
      <Tab.Screen name="Movies" component={MoviesStackNavigator} />
      <Tab.Screen name="TVShows" component={TVShowsStackNavigator} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
