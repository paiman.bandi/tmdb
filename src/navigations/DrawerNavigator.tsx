import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {MoviesStackNavigator, TVShowsStackNavigator} from './StackNavigator';
import TabNavigator from './TabNavigator';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={TabNavigator} />
      <Drawer.Screen name="Movies" component={MoviesStackNavigator} />
      <Drawer.Screen name="TV Shows" component={TVShowsStackNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
