#!/usr/bin/env bash

cat  << EOF
android.useAndroidX=true
android.enableJetifier=true
FLIPPER_VERSION=0.75.1
TMDB_RELEASE_STORE_FILE=tmdbkey.keystore
TMDB_RELEASE_STORE_PASSWORD=tmdbkey
TMDB_RELEASE_KEY_ALIAS=tmdbkey
TMDB_RELEASE_KEY_PASSWORD=tmdbkey
EOF
