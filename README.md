iOS

[![Build status](https://build.appcenter.ms/v0.1/apps/54be2d89-1a5d-4398-8606-a2f07a3c03dc/branches/dev/badge)](https://appcenter.ms)

Android

[![Build status](https://build.appcenter.ms/v0.1/apps/58171c35-e75b-49a6-a2b2-a739bdb166e5/branches/dev/badge)](https://appcenter.ms)

# TMDB

A catalog listing app for movies and TV shows built using React Native and The Movie Database (TMDB) API v3.

## Prerequisite

* Node
* yarn or npm

## Installation

```bash
git clone https://gitlab.com/paiman.bandi/tmdb.git
```

```bash
yarn
```

or 

```bash
npm install
```

## Run the project

```bash
yarn start
yarn android
yarn ios
```

or 

```bash
npm start
npm run android
npm run ios
```

## Test the project

```bash
yarn test
```

```bash
yarn test:watch
```

or 

```bash
npm run test
```

```bash
npm run test:watch
```


## Build APK

```bash
yarn gen-gradle-props

yarn build-apk
```

or

```bash
npm run gen-gradle-props

npm run build-apk
```

## The APK file Lacation

The APK file is located in android/app/build/outputs/apk/release/app-release.apk. 
